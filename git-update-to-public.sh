#!/bin/sh
 
if [ $# -ne 3 ]; then
    echo -e "\033[31m Usage:\033[0m ./git-update-to-public.sh src_branch dst_branch commit_message"
else
    src_branch="$1"
    dst_branch="$2"
    commit_message="$3"
    commit_tree=$(git commit-tree $src_branch^{tree} -p $(cat .git/refs/heads/$dst_branch) -m "$commit_message")
    echo $commit_tree
    git update-ref refs/heads/$dst_branch $commit_tree
fi
